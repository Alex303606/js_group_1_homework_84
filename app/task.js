const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Tasks = require('../models/Task');
const auth = require('../middleware/auth');
const config = require('../config');

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {
	// Product index
	router.get('/', auth, (req, res) => {
		Tasks.find().populate('user')
		.then(results => res.send(results))
		.catch(() => res.sendStatus(500));
	});
	
	// Product create
	router.post('/', [auth, upload.single('image')], (req, res) => {
		const taskData = req.body;
		
		if (req.file) {
			taskData.image = req.file.filename;
		} else {
			taskData.image = null;
		}
		
		if (!taskData.status || taskData.status !== 'new') taskData.status = 'new';
		
		taskData.user = req.user;
		
		const task = new Tasks(taskData);
		
		task.save()
		.then(result => res.send(result))
		.catch(error => res.status(400).send(error));
	});
	
	router.put('/:id', [auth, upload.single('image')], (req, res) => {
		const taskData = req.body;
		
		if (req.file) {
			taskData.image = req.file.filename;
		} else {
			taskData.image = null;
		}
		console.log(taskData.status);
		if(taskData.status && taskData.status !== 'new'){
			if(taskData.status === 'in_progress' || taskData.status === 'complete') {
				let myquery = {_id: req.params.id};
				let newvalues = {$set: {title: taskData.title, description: taskData.description, status: taskData.status}};
				Tasks.updateOne(myquery, newvalues, function (err, res) {
					if (err) res.status(400).send(error);
					console.log("1 document updated");
				}).then(result => res.send(result))
				.catch(error => res.status(400).send(error));
			} else {
				res.status(400).send('Status is wrong!');
			}
		} else {
			res.status(400).send('Status is wrong!');
		}
	});
	
	router.delete('/:id', auth, (req, res) => {
		Tasks.deleteOne({_id: req.params.id})
		.then(result => res.send(result))
		.catch(error => res.status(400).send(error));
	});
	
	return router;
};

module.exports = createRouter;