const express = require('express');
const bcrypt = require('bcrypt');
const User = require('../models/User');
const auth = require('../middleware/auth');
const nanoid = require('nanoid');

const createRouter = () => {
	const router = express.Router();
	
	router.post('/', (req, res) => {
		const user = new User(req.body);
		
		user.save()
		.then(user => res.send(user))
		.catch(error => res.status(400).send(error))
	});
	
	router.post('/sessions', async (req, res) => {
		const user = await User.findOne({username: req.body.username});
		
		if (!user) {
			return res.status(400).send({error: 'Username not found'});
		}
		
		// req.body.password
		const isMatch = await bcrypt.compare(req.body.password, user.password);
		
		if (!isMatch) {
			return res.status(400).send({error: 'Password is wrong!'});
		}
		user.token = nanoid();
		await user.save();
		return res.send({message: 'User and password correct!', user});
	});
	
	return router;
};

module.exports = createRouter;